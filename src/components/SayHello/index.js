// 导入组件，组件必须声明 name
import SayHello from './index.vue'

// 为组件提供 install 安装方法，供按需引入
SayHello.install = function (Vue) {
  Vue.component(SayHello.name, SayHello)
}
// 默认导出组件
export default SayHello
